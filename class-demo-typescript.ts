console.log("TypeScript");
class Person {

    private _name: string;

    constructor(name: string) {
        this._name = name;
    }

    talk(): void {
        console.log("Hello " + name);
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }
}


class Student extends Person {

    constructor(name: string) {
        super(name);
    }

    talk(): void {
        console.log("Hello Student " + name);
    }
}

let person: Person = new Person("Willi");
person.talk();

let student: Student = new Student("Martha");
student.talk();

