var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
console.log("TypeScript");
var Person = (function () {
    function Person(name) {
        this._name = name;
    }
    Person.prototype.talk = function () {
        console.log("Hello " + name);
    };
    Object.defineProperty(Person.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    return Person;
})();
var Student = (function (_super) {
    __extends(Student, _super);
    function Student(name) {
        _super.call(this, name);
    }
    Student.prototype.talk = function () {
        console.log("Hello Student " + name);
    };
    return Student;
})(Person);
var person = new Person("Willi");
person.talk();
var student = new Student("Martha");
student.talk();
//# sourceMappingURL=class-demo-typescript.js.map