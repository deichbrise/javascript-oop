console.log("ES6");

class Animal {

    // Constructor
    constructor(name) {
        this.name = name;
    }

    //  Klassenmethode
    talk() {
        console.log(this.name + ' talks');
    }

    // Statische Methode
    static getName() {
        return "Animal";
    }
}

// Vererbung
class Dog extends Animal {

    // Überschreiben
    talk() {

        // Zugriff auf die Superklasse mit super
        super.talk();
        console.log("wuff");
    }

    static getName() {
        return "Dog";
    }
}

// erzeugen der Objekte
var animal = new Animal("Cat");
var dog = new Dog("Wuffi");

// Aufrufen der Methode
animal.talk();
dog.talk();

var name1 = Animal.getName();
var name2 = Dog.getName();

console.log(name1, name2);



