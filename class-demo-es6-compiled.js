"use strict";

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

console.log("ES6");

var Animal = function () {

    // Constructor
    function Animal(name) {
        _classCallCheck(this, Animal);

        this.name = name;
    }

    //  Klassenmethode


    _createClass(Animal, [{
        key: "talk",
        value: function talk() {
            console.log(this.name + ' talks');
        }

        // Statische Methode

    }], [{
        key: "getName",
        value: function getName() {
            return "Animal";
        }
    }]);

    return Animal;
}();

// Vererbung


var Dog = function (_Animal) {
    _inherits(Dog, _Animal);

    function Dog() {
        _classCallCheck(this, Dog);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Dog).apply(this, arguments));
    }

    _createClass(Dog, [{
        key: "talk",


        // Überschreiben
        value: function talk() {

            // Zugriff auf die Superklasse mit super
            _get(Object.getPrototypeOf(Dog.prototype), "talk", this).call(this);
            console.log("wuff");
        }
    }], [{
        key: "getName",
        value: function getName() {
            return "Dog";
        }
    }]);

    return Dog;
}(Animal);

// erzeugen der Objekte


var animal = new Animal("Cat");
var dog = new Dog("Wuffi");

// Aufrufen der Methode
animal.talk();
dog.talk();

var name1 = Animal.getName();
var name2 = Dog.getName();

console.log(name1, name2);

//# sourceMappingURL=class-demo-es6-compiled.js.map