// 1. Namespace
console.log("ES5");
// global namespace
var VENDOR = VENDOR || {};

// Constructor
VENDOR.Person = function(name) {
    this.name = name;
    console.log("Constructor Person");
};

// Hinzufügen einer neuen Instanzmethode
VENDOR.Person.prototype.talk = function() {
    console.log("Say hello " + this.name);
};

// Hinzufügen einer statischen Methode
VENDOR.Person.getName = function() {
    console.log("Person");
};

// Erstellen eines neuen Objektes
var person1 = new VENDOR.Person("Willi");
person1.talk();

// Ausführen der statischen Methode
VENDOR.Person.getName();

// Ableitung der Klasse Person
VENDOR.Student = function(name, matrikelnummer) {
    VENDOR.Person.call(this, name);
    this.matrikelnummer = matrikelnummer;
    console.log("Constructor Student");
};

// Zuweisen der Person Classe zum prototype der Student Klasse
VENDOR.Student.prototype = Object.create(VENDOR.Person.prototype);

// Überschreiben der talk Methode
VENDOR.Person.prototype.talk = function() {
    console.log("Say hello to student " + this.name);
};

// Erzeugen eines neuen Studentobjektes
var student = new VENDOR.Student("Marta", 1727373);
student.talk();
